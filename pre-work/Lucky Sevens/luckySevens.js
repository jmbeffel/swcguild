function hideResults() {
    document.getElementById("resultDiv").style.display = "none";
} 
function play() {
    var startingBet = document.getElementById("inputBet").value;
    var bet = startingBet;
    var betArray = [bet];
//    betArray.push(bet);
    while (bet > 0) {
        var roll1 = Math.floor(Math.random(0,1) * 6) + 1;
        var roll2 = Math.floor(Math.random(0,1) * 6) + 1;
        var rollTotal = roll1 + roll2;
        if(rollTotal != 7) {
            bet -= 1;
        } else {
            bet += 4;
        }
        betArray.push(bet);
    }
    
    var rollCounter = betArray.length;
    var highestBet = Math.max(...betArray);
    var highestBetIndex = betArray.indexOf(highestBet) + 1;
    
    function showResults() {
        document.getElementById("resultDiv").style.display = "inline";
        document.getElementById("buttonPlay").value = "Play Again";
        document.getElementById("resultBet").innerHTML = "$" + startingBet + ".00";        
        document.getElementById("resultTotalRollCount").innerHTML = rollCounter;
        document.getElementById("resultHighestBet").innerHTML = "$" + highestBet + ".00"; 
        document.getElementById("resultHighestBetRollCount").innerHTML = highestBetIndex;
    }
    showResults();
}