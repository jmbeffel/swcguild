<div  class="container">
    <form class=form-horizontal" role="form" action="addNewDvd" method="POST">
        <div class="form-group">
            <label for="add-title" class="col-md-4 control-label">Title:</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="add-title" name="title" placeholder="Title"/>
            </div>
        </div>
        <div class="form-group">
            <label for="add-release-date" class="col-md-4 control-label">Release Date:</label>
            <div class="col-md-8">
                <input type="number" min="1900" max="2020" step="1" class="form-control" id="add-release-date" name="releaseDate" placeholder="Release Date"/>
            </div>
        </div>
        <div class="form-group">
            <label for="add-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
            <div class="col-md-8">
                <input type="number" min="0" max="5" step="1" class="form-control" id="add-mpaa-rating" name="mpaaRating" placeholder="MPAA Rating"/>
            </div>
        </div>
        <div class="form-group">
            <label for="add-director" class="col-md-4 control-label">Director:</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="add-director" name="director" placeholder="Director"/>
            </div>
        </div>
        <div class="form-group">
            <label for="add-studio" class="col-md-4 control-label">Studio:</label>
            <div class="col-md-8">
                <input type="text" class="form-control" id="add-studio" name="studio" placeholder="Studio"/>
            </div>
        </div>
        <div class="form-group">
            <label for="add-note" class="col-md-4 control-label">Note:</label>
            <div class="col-md-8">
                <textarea type="text" class="form-control" id="add-note" name="note" placeholder="Notes"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-8">
                <br>
                <button type="submit" id="add-button"
                        class="btn btn-default">Add New DVD</button>
            </div>
        </div>
    </form>
</div>