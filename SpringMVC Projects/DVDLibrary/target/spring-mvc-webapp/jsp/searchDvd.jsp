<%-- 
    Document   : searchDvd
    Created on : Oct 28, 2016, 6:57:34 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library - Search</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library - Search</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/listAllDvd">List Collection</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/searchDvd">Search</a>
                    </li>
                </ul>    
            </div>
            <h2>Search</h2>
            <form class="form-horizontal" role="form" action="searchDvds" method="POST">
                <div class="form-group">
                    <label for="search-title" class="col-md-4 control-label">Search DVDs for:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="search-title" name="title" placeholder="Title and/or Year (partial matches supported)"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-info">Search</button>
                    </div>
                </div>
            </form>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Title (Year) <c:if test="${not empty searchTerm}">Search results for: "${searchTerm}"</c:if> </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="dvd" items="${search}">
                        <s:url value="displayDvd" var="displayDvd_url">
                            <s:param name="dvdId" value="${dvd.dvdId}"/>
                        </s:url>
                        <tr>
                            <td>
                                <a class="btn" href="${displayDvd_url}">${dvd.title}</a><c:if test="${not empty dvd.releaseDate}">(${dvd.releaseDate})</c:if>
                                </td>
                            </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>