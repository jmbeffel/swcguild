/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  apprentice
 * Created: Nov 11, 2016
 */
drop database if exists dvd_library_test;
create database dvd_library_test;
use dvd_library_test;
drop table if exists Dvd;
create table `Dvd`(`DvdId` int primary key not null auto_increment,`Title` varchar(50) not null,`ReleaseDate` varchar(4) null,`MpaaRating` varchar(7) null,`Director` varchar(50) null,`Studio` varchar(50) null,`Note` varchar(100) null)engine=InnoDB auto_increment=1 default charset=latin1;
