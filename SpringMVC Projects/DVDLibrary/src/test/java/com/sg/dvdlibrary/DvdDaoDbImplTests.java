package com.sg.dvdlibrary;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sg.dvdlibrary.dao.DvdDao;
import com.sg.dvdlibrary.dto.Dvd;
import com.sg.dvdlibrary.dto.SearchTerm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author apprentice
 */
public class DvdDaoDbImplTests {
    
    private DvdDao dao;
    
    public DvdDaoDbImplTests() {
    }
 
    @Before
    public void setUp() {
        // Ask Spring for my DAO
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = (DvdDao) ctx.getBean("dvdDaoDbImpl");
        //grab the jdbc template to use for cleaning up
        //JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        //cleaner.execute("delete from Dvd");
        //load and run the sql file to start with a blank database, and start the Id tracker back to zero.
        dao.load("dvd_library_test");
        //JdbcTestUtils.executeSqlScript(cleaner, new ClassPathResource("/dvdLibraries/dvd_library_test.sql"), true);
    }
    
    @After
    public void tearDown(){
//        JdbcTestUtils.executeSqlScript(jdbcTemplate, new ClassPathResource("/dvdLibraries/dvd_library_test.sql"), true);
    }
    
    @Test
    public void testGetAllDvds(){
        //arrange
        ArrayList <Dvd> dvds = new ArrayList<>();
        //act
        dvds = dao.getAllDvds();
        //assert
        assertTrue("The loaded dvd_library isn't empty", dvds.isEmpty());
    }
    
    @Test
    public void testAddGetUpdateRemoveDvd(){
        //arrange
        Dvd test1 = new Dvd();
        //Dvd test2 = new Dvd();
        test1.setTitle("Star Wars");
        test1.setReleaseDate("1977");
        test1.setMpaaRating("");
        test1.setDirector("");
        test1.setStudio("");
        test1.setNote("");
        //act - add & get
        dao.addDvd(test1);
        Dvd test2 = dao.getDvdById(test1.getDvdId());
        //assert
        assertTrue("The added Dvd does not have the expected ID", test2.getDvdId()==test1.getDvdId());
        assertTrue("The added Dvd does not have the expected ID = 1", test2.getDvdId()==1);
        assertTrue("The added Dvd does not have the expected Title", test2.getTitle().equals(test1.getTitle()));
        assertTrue("The added Dvd does not have the expected Release Date", test2.getReleaseDate().equals(test1.getReleaseDate()));
        assertTrue("The added Dvd does not have the expected MPAA Rating", test2.getMpaaRating().equals(test1.getMpaaRating()));
        assertTrue("The added Dvd does not have the expected Director", test2.getDirector().equals(test1.getDirector()));
        assertTrue("The added Dvd does not have the expected Studio", test2.getStudio().equals(test1.getStudio()));
        assertTrue("The added Dvd does not have the expected Note", test2.getNote().equals(test1.getNote()));
        
        //arrange
        test1.setTitle("Impressive, Most Impressive.");
        test1.setReleaseDate("1980");
        test1.setMpaaRating("");
        test1.setDirector("");
        test1.setStudio("");
        test1.setNote("");
        //act - update
        dao.updateDvd(test1);
        //assert
        assertTrue("The updated Dvd did not have the expected Title",dao.getDvdById(test1.getDvdId()).getTitle().equals(test1.getTitle()));
        assertTrue("The updated Dvd did not have the expected Release Date",dao.getDvdById(test1.getDvdId()).getReleaseDate().equals(test1.getReleaseDate()));
        assertTrue("The updated Dvd did not have the expected MPAA Rating",dao.getDvdById(test1.getDvdId()).getMpaaRating().equals(test1.getMpaaRating()));
        assertTrue("The updated Dvd did not have the expected Director",dao.getDvdById(test1.getDvdId()).getDirector().equals(test1.getDirector()));
        assertTrue("The updated Dvd did not have the expected Studio",dao.getDvdById(test1.getDvdId()).getStudio().equals(test1.getNote()));
        assertTrue("The updated Dvd did not have the expected Note",dao.getDvdById(test1.getDvdId()).getNote().equals(test1.getNote()));
        //act - remove
        dao.removeDvd(test1.getDvdId());
        //assert
        assertNull("The Dvd to be removed remains in the database", dao.getDvdById(test1.getDvdId()));
    }
    
    @Test
    public void testLoadDefaultCollectionAndSearch(){
        //arrange
        //act - load
        dao.load("dvd_default_library");
        ArrayList<Dvd> dvds = dao.getAllDvds();
        //assert
        assertTrue("The default library does not have 13 items", dvds.size()==13);
        //arrange - search by title
        List<Dvd> results;
        Map<SearchTerm, String> criteria = new HashMap<>();
        criteria.put(SearchTerm.TITLE, "star wars");
        //act
        results = dao.searchDvds(criteria);
        //assert
        assertTrue("The search results for 'star wars' is not 7 items.", results.size()==7);
        //arrange - search by release date
        criteria.put(SearchTerm.TITLE, "2014");
        //act
        results = dao.searchDvds(criteria);
        //assert
        assertTrue("The search results for '2014' is not 2 items.", results.size()==2);
        //arrange - search by release date and title
        criteria.put(SearchTerm.TITLE, "lego 2014");
        //act
        results = dao.searchDvds(criteria);
        //assert
        assertTrue("The search results for 'lego 2014' is not 1 item.", results.size()==1);
        assertTrue("The search results for 'lego 2014' is not the dvd with title: 'Lego Movie'", results.get(0).getTitle().equals("Lego Movie"));
    }
    
}
