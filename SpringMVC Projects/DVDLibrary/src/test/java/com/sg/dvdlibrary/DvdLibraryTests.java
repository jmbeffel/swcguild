/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary;

import com.sg.dvdlibrary.dao.DvdDao;
import com.sg.dvdlibrary.dao.DvdDaoMemImpl;
import com.sg.dvdlibrary.dto.Dvd;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DvdLibraryTests {
    
    public DvdLibraryTests() {
    }
    
    DvdDao dao;
    
    @Before
    public void setUp(){
        //dependency injection for the tests
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = (DvdDao) ctx.getBean("dvdDaoMemImpl");
    }
    
    //tests for all the main DAO methods:
    //add
    @Test
    public void testAddDvd(){
        //arrange
        Dvd test1 = new Dvd();
        //act
        dao.addDvd(test1);
        //assert
        assertTrue("The DAO does not assign the proper ID before adding a new dvd.",test1.getDvdId()==1);
        assertTrue("The collection of dvds is not 1 after adding 1 dvd to an empty hashmap", dao.getAllDvds().size()==1);
        assertTrue("The dvd added to the collection was not the same as the dvd in the collection",dao.getAllDvds().get(0)==test1);
    }
    //getall
    @Test
    public void testGetAllDvds(){
        //arrange
        //act
        //assert
        assertTrue("Loading an empty file does not produce an empty HashMap", dao.getAllDvds().isEmpty());
    }
    //edit
    @Test
    public void testEditDvd(){
        //arrange
        Dvd test1 = new Dvd();
        test1.setTitle("Star Wars");
        dao.addDvd(test1);
        test1.setTitle("Empire Strikes Back");
        //act
        dao.updateDvd(test1);
        //assert
        assertTrue("The updated title is not the expected result", dao.getAllDvds().get(0).getTitle().equals("Empire Strikes Back"));
        assertFalse("The updated title did not change", dao.getAllDvds().get(0).getTitle().equals("Star Wars"));
    }
    //remove
    @Test
    public void testRemoveDvd(){
        //arrange
        Dvd test1 = new Dvd();
        dao.addDvd(test1);
        //act
        dao.removeDvd(1);
        //assert
        assertTrue("removing the only dvd does not produce an empty HashMap", dao.getAllDvds().isEmpty());
    }
    //perform the actions multiple times
    @Test
    public void testAddAndRemoveMultipleDvds(){
        //arrange
        Dvd test1 = new Dvd();
        Dvd test2 = new Dvd();
        Dvd test3 = new Dvd();
        //act
        dao.addDvd(test1);
        dao.addDvd(test2);
        dao.addDvd(test3);
        //assert
        assertTrue("All the dvds were not added to the library in memory", dao.getAllDvds().size()==3);
        assertTrue("The ID of the third dvd added to an empty collection is not 3", dao.getAllDvds().get(2).getDvdId()==3);
        //act
        dao.removeDvd(2);
        //assert
        assertTrue("Removing one dvd from a collection of 3 does not record a new size of 2", dao.getAllDvds().size()==2);
        assertTrue("The remaining IDs are not 1 & 3", dao.getAllDvds().get(0).getDvdId()==1&&dao.getAllDvds().get(1).getDvdId()==3);
        //act
        dao.removeDvd(1);
        dao.removeDvd(3);
        //assert
        assertTrue("removing the remaining dvds does not give an empty HashMap", dao.getAllDvds().isEmpty());
        
    }
    //load
    @Test
    public void testLoad(){
        //arrange
        //act
        dao.load("dvdLibrary");
        //assert
        assertTrue("ID loaded into memory is not the same as the value from file", dao.getAllDvds().get(0).getDvdId()==7);
        assertTrue("Title loaded into memory is not the same as the value from file", dao.getAllDvds().get(0).getTitle().equals("Empire Strikes Back"));
        assertTrue("Date loaded into memory is not the same as the value from file", Integer.parseInt(dao.getAllDvds().get(0).getReleaseDate())==1980);
        assertTrue("MPAA loaded into memory is not the same as the value from file", dao.getAllDvds().get(0).getMpaaRating().equals("5"));
        assertTrue("Director loaded into memory is not the same as the value from file", dao.getAllDvds().get(0).getDirector().equals("Lucas"));
        assertTrue("Studio loaded into memory is not the same as the value from file", dao.getAllDvds().get(0).getStudio().equals("Lucas Films"));
        assertTrue("Note loaded into memory is not the same as the value from file", dao.getAllDvds().get(0).getNote().equals("Go Vader!"));
    }
    //save
//    @Test
//    public void testSave(){
//        //arrange
//        //act
//        //assert
//    }
    
}
