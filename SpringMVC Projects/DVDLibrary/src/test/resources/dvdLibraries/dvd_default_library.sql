/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  apprentice
 * Created: Nov 11, 2016
 */
drop database if exists dvd_library_test;
create database dvd_library_test;
use dvd_library_test;
drop table if exists Dvd;
create table `Dvd`(`DvdId` int primary key not null auto_increment,`Title` varchar(50) not null,`ReleaseDate` varchar(4) null,`MpaaRating` varchar(7) null,`Director` varchar(50) null,`Studio` varchar(50) null,`Note` varchar(100) null)/*engine=InnoDB auto_increment=1 default charset=latin1*/;
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Star Wars Episode IV: A New Hope',1977,'PG','Lucas','Lucas Films','Millennium Falcon!');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Star Wars Episode V: Empire Strikes Back',1980,'PG','Irvin Kershner','Lucas Films','Go Vader!');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Star Wars Episode VI: Return of the Jedi',1983,'PG','Richard Marquand','Lucas Films','Ewoks!');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Star Wars Episode I: The Phantom Menace',1999,'PG','Lucas','Lucas Films','Pod Racing?');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Star Wars Episode II: Attack of the Clones',2002,'PG','Lucas','Lucas Films','Boba Fett');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Star Wars Episode III: Revenge of the Sith',2005,'PG-13','Lucas','Lucas Films','Balance the Force');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Star Wars Episode VII: The Force Awakens',2015,'PG-13','J.J Abrams','Lucas Films','Bye Solo...');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Guardians of the Galaxy',2014,'PG-13','James Gunn','Marvel Studios','Oooo Child');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Avengers','2012','PG-13','Joss Whedon','Marvel Studios','Hulk, Smash');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Tron',1982,'PG','Steven Lisberger','Disney','user');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Tron: Legacy',2010,'PG','Joseph Kosinski','Disney','Music by Daft Punk');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Lego Movie',2014,'PG','Phil Lord, Christopher Miller, Chris Mckay','Warner Bros. Pictures','Everything is Awesome');
insert into `dvd_library_test`.`Dvd` (`Title`, `ReleaseDate`, `MpaaRating`, `Director`, `Studio`, `Note`) values('Inside Out',2015,'PG','Peter Doctor, Ronnie del Carmen','Disney/Pixar','Whirlwind of Emotions');