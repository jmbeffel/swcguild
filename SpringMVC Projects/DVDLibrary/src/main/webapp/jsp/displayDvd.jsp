<%-- 
    Document   : displayDvd
    Created on : Oct 28, 2016, 6:58:19 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library - Display Info</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library - Display Info</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/listAllDvd">List Collection</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/searchDvd">Search</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="#">Display Info</a>
                    </li>
                </ul>    
            </div>
            <div class="row">
                <h2><c:out value="${dvd.title}"/></h2>
                <label for="displayReleaseDate" class="col-md-4">Release Date:</label>
                <div id="displayReleaseDate" class="col-md-8"><c:out value="${dvd.releaseDate}"/></div>
            </div>
            <div class="row">

                <label for="displayMpaaRating" class="col-md-4">MPAA Rating(String):</label> 
                <div id="displayMpaaRating" class="col-md-8"><c:out value="${dvd.mpaaRating}"/></div>
            </div>
            <div class="row">
                <label for="displayDirector" class="col-md-4">Director:</label> 
                <div id="displayDirector" class="col-md-8"><c:out value="${dvd.director}"/></div>
            </div>
            <div class="row">

                <label for="displayStudio" class="col-md-4">Studio:</label> 
                <div id="displayStudio" class="col-md-8"><c:out value="${dvd.studio}"/></div>
            </div>
            <div class="row">

                <label for="displayNote" class="col-md-4">Note:</label> 
                <div id="displayNote" class="col-md-8"><c:out value="${dvd.note}"/></div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.0.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>