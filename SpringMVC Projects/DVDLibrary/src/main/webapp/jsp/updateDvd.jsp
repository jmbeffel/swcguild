<%-- 
    Document   : updateDvd
    Created on : Oct 28, 2016, 6:56:54 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library - Update</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library - Update</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/listAllDvd">List Collection</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/searchDvd">Search</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/updateDvd">Update</a>
                    </li>
                </ul>    
            </div>
            <h2>Update</h2>
            <sf:form class="form-horizontal" role="form" modelAttribute="dvd" action="updateDvd" method="POST">
                <div class="form-group">
                    <label for="update-title" class="control-label col-md-4">Title:</label>
                    <div class="col-md-8">
                        <sf:input path="title" type="text" class="form-control" id="update-title" name="title" placeholder="${dvd.title}"/>
                        <sf:errors path="title" cssClass="bg-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="update-release-date" class="control-label col-md-4">Release Date::</label>
                    <div class="col-md-8">
                        <sf:input path="releaseDate" type="text" class="form-control" id="update-release-date" name="releaseDate" placeholder="1900, 1977, 2014, etc." />
                        <sf:errors path="releaseDate" cssClass="bg-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="update-mpaa-rating" class="control-label col-md-4">MPAA Rating (String):</label>
                    <div class="col-md-8">
                        <sf:input path="mpaaRating" type="text" class="form-control" id="update-mpaa-rating" name="mpaaRating" placeholder="G, PG, PG-13, etc." />
                        <sf:errors path="mpaaRating" cssClass="bg-danger"/>
                    </div>
                </div>
                <!--                    <div class="form-group">
                                    <label for="update-rating" class="control-label col-md-4">MPAA Rating (Enum):</label>
                                    <div class="col-md-8">
                <//sf:input path="rating.label" type="text" class="form-control" id="update-rating" name="rating" placeholder="G, PG, PG-13, etc." />
                <//sf:errors path="rating.label" cssClass="bg-danger"/>
            </div>
        </div>-->
                <div class="form-group">
                    <label for="update-director" class="control-label col-md-4">Director:</label>
                    <div class="col-md-8">
                        <sf:input path="director" type="text" class="form-control" id="update-director" name="director" placeholder="Director" />
                        <sf:errors path="director" cssClass="bg-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="update-studio" class="control-label col-md-4">Studio:</label>
                    <div class="col-md-8">
                        <sf:input path="studio" type="text" class="form-control" id="update-studio" name="studio" placeholder="Studio" />
                        <sf:errors path="studio" cssClass="bg-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="update-note" class="control-label col-md-4">Note:</label>
                    <div class="col-md-8">
                        <sf:input path="note" type="text" class="form-control" id="update-note" name="note" placeholder="Notes about ${dvd.title}" />
                        <sf:errors path="note" cssClass="bg-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <sf:hidden path="dvdId"/>
                        <button type="submit" id="update-button" class="btn btn-warning">Update DVD</button>
                    </div>
                </div>
            </sf:form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>