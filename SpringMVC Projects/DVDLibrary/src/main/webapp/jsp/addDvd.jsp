<%-- 
    Document   : addDvd
    Created on : Oct 28, 2016, 6:56:22 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library - Add New</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library - Add New</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/listAllDvd">List Collection</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/searchDvd">Search</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/addDvd">Add New</a>
                    </li>
                </ul>    
            </div>
            <h2>Add New DVD to Collection</h2>
            <div  class="container">
                <sf:form class="form-horizontal" role="form" action="addDvd" modelAttribute="dvd" method="POST">
                    <div class="form-group">
                        <label for="add-title" class="col-md-4 control-label">Title:</label>
                        <div class="col-md-8">
                            <sf:input path="title" type="text" class="form-control" id="add-title" name="title" placeholder="Title"/>
                            <sf:errors path="title" cssClass="bg-danger"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-release-date" class="col-md-4 control-label">Release Date:</label>
                        <div class="col-md-8">
                            <input type="number" min="0" max="2021" step="1" class="form-control" id="add-release-date" name="releaseDate" placeholder="Release Date"/>
                            <sf:errors path="releaseDate" cssClass="bg-danger"/>
                        </div>
                    </div>
                    <!--                    <div class="form-group">
                                            <label for="add-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
                                            <div class="col-md-8">
                                                <//sf:select path="rating" type="text" class="form-control" id="add-rating" name="rating" placeholder="MPAA Rating" items="$/{mpaaRatings}" itemLabel="label" itemValue="value"/>
                                                <//sf:errors path="rating" cssClass="bg-danger"/>
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <label for="add-mpaa-rating" class="col-md-4 control-label">MPAA Rating (String):</label>
                        <div class="col-md-8">
                            <sf:input path="mpaaRating" type="text" class="form-control" id="add-mpaa-rating" name="mpaaRating" placeholder="MPAA Rating (Free Response)"/>
                            <sf:errors path="mpaaRating" cssClass="bg-danger"/>
                        </div>
                    </div>
                    <!--                    <div class="form-group">
                                            <label for="add-mpaa-rating" class="col-md-4 control-label">MPAA Rating (ENUM):</label>
                                            <div class="col-md-8">
                    <//sf:input path="rating.label" type="text" class="form-control" id="add-rating" name="rating" placeholder="MPAA Rating (Enum)"/>
                    <//sf:errors path="rating.label" cssClass="bg-danger"/>
                </div>
            </div>-->
                    <div class="form-group">
                        <label for="add-director" class="col-md-4 control-label">Director:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-director" name="director" placeholder="Director"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-studio" class="col-md-4 control-label">Studio:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-studio" name="studio" placeholder="Studio"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-note" class="col-md-4 control-label">Note:</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" id="add-note" name="note" placeholder="Notes"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <br>
                            <button type="submit" id="add-button"
                                    class="btn btn-default">Add New DVD</button>
                        </div>
                    </div>
                </sf:form>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>