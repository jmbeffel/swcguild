<%-- 
    Document   : home
    Created on : Oct 28, 2016, 6:55:19 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library - Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/starWars/Darth-Vader-icon.png">
        <style>

        </style>
    </head>
    <body>
        <div class="container">
            <h1>DVD Library - Home</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/listAllDvd">List Collection</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/searchDvd">Search</a>
                    </li>
                </ul>    
            </div>
            <h2>DVD SQL Database - Populated With Ajax</h2>
            <div class="col-md-7">
                <table id="dvdTable" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th><h3>Title (Year)</h3></th>
                            <th><h3>Rating</h3></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="contentRows"></tbody>
                </table>
            </div>
            <div id="homePageDisplay" class="col-md-5">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="#home_dvd_add" aria-controls="addDvd" role="tab" data-toggle="tab">Add Dvd</a></li>
                    <li role="presentation" class="active"><a href="#home_dvd_info" aria-controls="displayDvd" role="tab" data-toggle="tab">Dvd Info</a></li>
                    <li role="presentation"><a href="#home_dvd_search" aria-controls="searchDvds" role="tab" data-toggle="tab">Search Dvds</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" id="home_dvd_add" class="tab-pane">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-title" class="col-sm-3 control-label">Title:</label>
                                <div class="col-sm-9">
                                    <input path="title" type="text" class="form-control" id="home-add-title" name="title" placeholder="Title"/>
                                    <!--<errors path="title" cssClass="bg-danger"/>-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-release-date" class="col-sm-3 control-label">Release Date:</label>
                                <div class="col-sm-9">
                                    <input type="number" min="1900" max="2021" step="1" class="form-control" id="home-add-release-date" name="releaseDate" placeholder="Release Date"/>
                                    <!--<errors path="releaseDate" cssClass="bg-danger"/>-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-mpaa-rating" class="col-sm-3 control-label">MPAA Rating:</label>
                                <div class="col-sm-9">
                                    <input path="mpaaRating" type="text" class="form-control" id="home-add-mpaa-rating" name="mpaaRating" placeholder="MPAA Rating (Free Response)"/>
                                    <!--<errors path="mpaaRating" cssClass="bg-danger"/>-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-director" class="col-sm-3 control-label">Director:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="home-add-director" name="director" placeholder="Director"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-studio" class="col-sm-3 control-label">Studio:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="home-add-studio" name="studio" placeholder="Studio"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-note" class="col-sm-3 control-label">Note:</label>
                                <div class="col-sm-9">
                                    <textarea type="text" class="form-control" id="home-add-note" name="note" placeholder="Notes"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <br>
                                    <button type="submit" id="home-add-button"
                                            class="btn btn-success">Add New DVD</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" id="home_dvd_info" class="tab-pane active">
                        Click on a Dvd Title for more information.
                    </div>
                    <div role="tabpanel" id="home_dvd_search" class="tab-pane">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="search-title" class="col-md-4 control-label">Search DVDs for:</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="home-search-title" name="title" placeholder="Title and/or Year (partial matches supported)"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button id="home-search-button" type="submit" class="btn btn-info">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="home-edit-modal" tabindex="-1" role="dialog" aria-labelledby="editDetailsModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="editDetailsModalLabel">Edit DVD</h4>
                        </div>
                        <div class="modal-body">
                            <h2>Edit DVD</h2>
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="home-edit-title" class="col-md-4 control-label">Title:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="home-edit-title" placeholder="Title" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="home-edit-release-date" class="col-md-4 control-label">Release Date:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="home-edit-release-date" placeholder="Release Date" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="home-edit-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="home-edit-mpaa-rating" placeholder="MPAA Rating" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="home-edit-director" class="col-md-4 control-label">Director:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="home-edit-director" placeholder="Director" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="home-edit-studio" class="col-md-4 control-label">Studio:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="home-edit-studio" placeholder="Studio" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="home-edit-note" class="col-md-4 control-label">Note:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="home-edit-note" placeholder="Note" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" id="home-edit-button" class="btn btn-default" data-dismiss="modal">Edit DVD</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <input type="hidden" id="home-edit-dvd-id" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<a type="button" class="btn btn-success" name="addDvd" href="${pageContext.request.contextPath}/addDvd">Add DVD</a>-->
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/dvdLibrary.js"></script>

</body>
</html>

