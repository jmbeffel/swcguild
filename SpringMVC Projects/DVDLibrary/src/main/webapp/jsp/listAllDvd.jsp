<%-- 
    Document   : listAllDvd
    Created on : Oct 28, 2016, 6:57:15 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library - List Collection</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library - List Collection</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/listAllDvd">List Collection</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/searchDvd">Search</a>
                    </li>
                </ul>    
            </div>
            <!--<a type="button" class="btn btn-success" name="addDvd" href="${pageContext.request.contextPath}/addDvd">Add DVD</a>-->
            <!--<a type="button" class="btn btn-warning" name="addDvd" href="${pageContext.request.contextPath}/updateDvd">Update DVD</a>-->
            <!--<a type="button" class="btn btn-danger" name="addDvd" href="${pageContext.request.contextPath}/removeDvd">Remove DVD</a>-->
            <c:if test="${fn:length(dvdLibrary) lt 1}">
                <form action="loadDefaultDvds" method="POST">
                    <button type="submit" class="btn btn-default">Load Default Collection</button>
                </form>
            </c:if>
            <c:if test="${fn:length(dvdLibrary) gt 0}">
                <form action="sortDvds">
                    <div class="form-group">
                        <label for="sortButtons">Sort by:</label> 
                        <span id="sortButtons">
                            <button class="btn btn-primary" type="submit" value="Title" name="sortBy">Title</button> <button class="btn btn-info" type="submit" value="ReleaseDate" name="sortBy">Year</button>
                        </span>
                    </div>
                </form>
            </c:if>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Title (Year)</th>
                        <th>Rating</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="dvd" items="${dvdLibrary}">
                        <s:url value="displayUpdateDvd" var="updateDvd_url">
                            <s:param name="dvdId" value="${dvd.dvdId}"/>
                        </s:url>
                        <s:url value="removeDvd" var="deleteDvd_url">
                            <s:param name="dvdId" value="${dvd.dvdId}"/>
                        </s:url>
                        <s:url value="displayDvd" var="displayDvd_url">
                            <s:param name="dvdId" value="${dvd.dvdId}"/>
                        </s:url>
                        <tr>
                            <td>
                                <a class="btn" href="${displayDvd_url}">${dvd.title}</a><c:if test="${not empty dvd.releaseDate}">(${dvd.releaseDate})
                                </c:if>
                            </td>
                            <td>
                                ${dvd.mpaaRating}
                            </td>
                            <td class="row text-right">
                                <a class="btn btn-warning" href="${updateDvd_url}">Edit</a>  
                                <a class="btn btn-danger col-xs-offset-2" href="${deleteDvd_url}">Delete</a>
                            </td>

                        </tr>
                    </c:forEach>
                </tbody>
                <tfoot>    
                    <tr>
                        <td>
                            <a type="button" class="btn btn-success" name="addDvd" href="${pageContext.request.contextPath}/addDvd">Add DVD</a>
                        </td>
                    </tr>    
                </tfoot>
            </table>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>