/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//when the doc is ready, fill the table with all the DVDs
$(document).ready(function () {
    loadDvds();

    $('#home-add-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'dvd',
            data: JSON.stringify({
                title: $('#home-add-title').val(),
                releaseDate: $('#home-add-release-date').val(),
                mpaaRating: $('#home-add-mpaa-rating').val(),
                director: $('#home-add-director').val(),
                studio: $('#home-add-studio').val(),
                note: $('#home-add-note').val()
            }),
            contentType: 'application/json; charset=utf-8',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            dataType: 'json',
            success: function () {
                $('#home-add-title').val('');
                $('#home-add-release-date').val('');
                $('#home-add-mpaa-rating').val('');
                $('#home-add-director').val('');
                $('#home-add-studio').val('');
                $('#home-add-note').val('');
                
            //need to reload the table after the dvd has been added
            loadDvds();
            }
            //need to start making the validation errors for this
            //haven't checked if the errors work here at all
            //OR: I see if I can somehow combo this with JSTL for the error checking...
            
        });
    });
    
    $('#home-edit-button').click(function(event){
       event.preventDefault();
       
       $.ajax({
           type: 'PUT',
           url: 'dvd/'+ $('#home-edit-dvd-id').val(),
           data: JSON.stringify({
               dvdId: $('#home-edit-dvd-id').val(),
               title: $('#home-edit-title').val(),
               releaseDate: $('#home-edit-release-date').val(),
               mpaaRating: $('#home-edit-mpaa-rating').val(),
               director: $('#home-edit-director').val(),
               studio: $('#home-edit-studio').val(),
               note: $('#home-edit-note').val()
           }),
           headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json'
           },
           'dataType': 'json',
           success: function(){
               loadDvds();
           }
       });
    });
    
    //need to add edit function (modal?)
    //want to add sorting by title and/or year
    //need to add searching...starting with by title, then adding extra parts.
    $('#home-search-button').click(function(event){
        event.preventDefault();
        $.ajax({
            method:'POST',
            url:'search/dvds',
            dataType:'json',
            data: JSON.stringify({
                title: $('#home-search-title').val()
            }),
            headers:{
                'Accept': 'application/json',
                'Content-Type':'application/json'
            },
            success: function(searchResults, status) {
                $('#home-search-title').val('');
                fillDvdTable(searchResults, status);
            }
        });
    });
});

//load the dvds into the table
function loadDvds() {
    //get the JSON object from the endpoint
    $.ajax({
        url: 'dvds'
    }).success(function (data, status) {
        fillDvdTable(data, status);
    });
}
//fill the table with dvds
function fillDvdTable(dvdList, status) {
    //clear any previously shown information
    clearDvdTable();

    //store the tbody in a variable
    var summaryTable = $('#contentRows');

    $.each(dvdList, function (arrayPosition, dvd) {
        summaryTable.append($('<tr>')
                .append($('<td>')
                        .attr({
                            'onClick': 'displayDvdInfo(' + dvd.dvdId + ')'
                        })
                        .append($('<a>')
                                .attr({
                                    'onClick': 'displayDvdInfo(' + dvd.dvdId + ')'
                                })
                                .text(dvd.title))
                        .append(' (' + dvd.releaseDate + ')'))
                .append($('<td>').text(dvd.mpaaRating))
                .append($('<td>')
                        .append($('<a class="btn btn-warning">')
                                .attr({
                                    //need to make this the edit dvds
                                    //'onClick': 'editContact(' + dvd.dvdId + ')'
                                    'data-dvd-id':dvd.dvdId,
                                    'data-toggle':'modal',
                                    'data-target':'#home-edit-modal'
                                })
                                .text('Edit')))
                .append($('<td>')
                        .append($('<a class="btn btn-danger">')
                                .attr({
                                    'onClick': 'deleteDvd(' + dvd.dvdId + ')'
                                })
                                .text('Delete'))));
    });
}

//clear all the rows from the table
function clearDvdTable() {
    $('#contentRows').empty();
}

function deleteDvd(id) {
    var answer = confirm('Confirm Delete?');

    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id
        }).success(function () {
            // reload list of dvds
            loadDvds();
        });
    }
}

function displayDvdInfo(id) {
    $.ajax({
        url: 'dvd/' + id
    }).success(function (data, status) {
        $('.nav-tabs a[href="#home_dvd_info"]').tab('show');
        fillDvdInfoDisplay(data, status);
    });

}

function fillDvdInfoDisplay(dvd, status) {
    clearInfoDisplay();
    var display = $('#home_dvd_info');
    var dvdNote = dvd.note;

    $.ajax({
        type: 'GET',
        url: dvdNote,
        success: function (json) {
            $("#home_display_dvd_note").html(json.opening_crawl);
        }
    });

    display.append($('<table class="table table-hover">')
            .append($('<thead>')
                    .append($('<tr>')
                            .append($('<th>')
                                    .attr({
                                        'width': '40%'
                                    }))
                            .append($('<th>')
                                    .attr({
                                        'width': '60%'
                                    }))))
            .append($('<tbody>')
                    .append($('<tr>')
                            .append($('<td>')
                                    .attr({
                                        'colspan': '2'
                                    })
                                    .append($('<h4>').text(dvd.title))))
                    .append($('<tr>')
                            .append($('<td>')
                                    .text('Release Date:'))
                            .append($('<td>')
                                    .text(dvd.releaseDate)))
                    .append($('<tr>')
                            .append($('<td>')
                                    .text('MPAA Rating:'))
                            .append($('<td>')
                                    .text(dvd.mpaaRating)))
                    .append($('<tr>')
                            .append($('<td>')
                                    .text('Director:'))
                            .append($('<td>')
                                    .text(dvd.director)))
                    .append($('<tr>')
                            .append($('<td>')
                                    .text('Studio:'))
                            .append($('<td>')
                                    .text(dvd.studio)))
                    .append($('<tr>')
                            .append($('<td>')
                                    .text('Note:'))
                            .append($('<td id="home_display_dvd_note">')
                                    .text(dvdNote)))));

}

function clearInfoDisplay() {
    $('#home_dvd_info').empty();
}

$('#home-edit-modal').on('show.bs.modal', function(event){
    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');
    var modal = $(this);
    
    $.ajax({
        type:'GET',
        url:'dvd/'+ dvdId,
        success: function(sampleEditDvd) {
            modal.find('#home-edit-dvd-id').val(sampleEditDvd.dvdId);
            modal.find('#home-edit-title').val(sampleEditDvd.title);
            modal.find('#home-edit-release-date').val(sampleEditDvd.releaseDate);
            modal.find('#home-edit-mpaa-rating').val(sampleEditDvd.mpaaRating);
            modal.find('#home-edit-director').val(sampleEditDvd.director);
            modal.find('#home-edit-studio').val(sampleEditDvd.studio);
            modal.find('#home-edit-note').val(sampleEditDvd.note);
        }
    });
});
