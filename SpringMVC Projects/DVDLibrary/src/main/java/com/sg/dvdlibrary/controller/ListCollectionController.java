/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.controller;

import com.sg.dvdlibrary.dao.DvdDao;
import com.sg.dvdlibrary.dao.DvdDaoDbImpl;
import com.sg.dvdlibrary.dto.Dvd;
import com.sg.dvdlibrary.dto.MpaaRating;
import com.sg.dvdlibrary.ops.DvdComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class ListCollectionController {

    private DvdDao dao;
    private DvdComparator dvdCompare;

    @Inject
    public ListCollectionController(DvdDao dao) {
        this.dao = dao;
        dvdCompare = new DvdComparator();
    }
    
    //    In this lab, you will create a program that stores a DVD collection. The program must:
    

//1. Allow the user to add a DVD to the collection
    @RequestMapping(value = "/addDvd", method = RequestMethod.GET)
    public String addDvd(Model model) {
        Dvd dvd = new Dvd();
        model.addAttribute("dvd", dvd);
        model.addAttribute("mpaaRatings", MpaaRating.values() );
        return "addDvd";
    }

    @RequestMapping(value = "/addDvd", method = RequestMethod.POST)
    public String addDvd(HttpServletRequest req, @Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result) {
        String title = req.getParameter("title");
        String releaseDate = req.getParameter("releaseDate");
        try {
            Integer.parseInt(releaseDate);
        } catch (NumberFormatException nfe) {
            releaseDate = null;
        }
        String mpaaRating = req.getParameter("mpaaRating");
        String director = req.getParameter("director");
        String studio = req.getParameter("studio");
        String note = req.getParameter("note");

        if (result.hasErrors()) {
            return "addDvd";
        }
        
        dvd.setTitle(title);
//        try{
        dvd.setReleaseDate(releaseDate);
//        }catch(NumberFormatException nf){
        //not an integer.
//        }
        dvd.setMpaaRating(mpaaRating);
        dvd.setDirector(director);
        dvd.setStudio(studio);
        dvd.setNote(note);
        
        dao.addDvd(dvd);

        return "redirect:listAllDvd";
    }
//2. Allow the user to remove a DVD from the collection

    @RequestMapping(value = "/removeDvd", method = RequestMethod.GET)
    public String removeDvd(HttpServletRequest req) {
        int dvdId = Integer.parseInt(req.getParameter("dvdId"));
        dao.removeDvd(dvdId);
        return "redirect:listAllDvd";
    }
//3. Allow the user to list the DVDs in the collection

    @RequestMapping(value = "/listAllDvd", method = RequestMethod.GET)
    public String listAllDvd(HttpServletRequest req, Model model) {
        List<Dvd> dvdLibrary = dao.getAllDvds();
        //dvdCompare.setSortBy(DvdComparator.Order.Title);
        Collections.sort(dvdLibrary, dvdCompare);
        model.addAttribute("dvdLibrary", dvdLibrary);
        return "listAllDvd";
    }
//4. Allow the user to display the information for a particular DVD

    @RequestMapping(value = "/displayDvd", method = RequestMethod.GET)
    public String displayDvd(HttpServletRequest req, Model model) {
        int dvdId = Integer.parseInt(req.getParameter("dvdId"));
        Dvd dvd = dao.getDvdById(dvdId);
        model.addAttribute("dvd",dvd);
        return "displayDvd";
    }

//7. Save the DVD library back to the file when the program completes

    @RequestMapping(value = "/saveDvd", method = RequestMethod.GET)
    public String save(HttpServletRequest req) {
        return "saveDvd";
    }
//8. Allows the user to add/edit/delete many DVDs in one session

    @RequestMapping(value = "/displayUpdateDvd", method = RequestMethod.GET)
    public String displayUpdateDvd(HttpServletRequest req, Model model) {
        int dvdId = Integer.parseInt(req.getParameter("dvdId"));
        Dvd dvd = dao.getDvdById(dvdId);
        model.addAttribute("ratings",MpaaRating.values());
        model.addAttribute(dvd);
        return "updateDvd";
    }
    
    @RequestMapping(value = "/updateDvd", method = RequestMethod.POST)
    public String updateDvd(@Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result) {
        //int dvdId = Integer.parseInt(req.getParameter("dvdId"));
        if(result.hasErrors()){
            return "updateDvd";
        }
        dao.updateDvd(dvd);
        return "redirect:listAllDvd";
    }

    @RequestMapping(value = "/loadDefaultDvds", method = RequestMethod.POST)
    public String loadDefaultDvds() {
        dao.load("dvdLibrary");
        return "redirect:listAllDvd";
    }
    
    @RequestMapping(value = "/sortDvds", method = RequestMethod.GET)
    public String sortDvds(HttpServletRequest req, Model model){
        String sortBy = req.getParameter("sortBy");
        dvdCompare.setSortBy(DvdComparator.getOrderFromString(sortBy));
        return listAllDvd(req, model);
    }
}
