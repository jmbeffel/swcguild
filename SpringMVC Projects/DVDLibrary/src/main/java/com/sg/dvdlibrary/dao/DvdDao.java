/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.dao;

import com.sg.dvdlibrary.dto.SearchTerm;
import com.sg.dvdlibrary.dto.Dvd;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DvdDao {

//CRUD

    // C
    public Dvd addDvd(Dvd dvd);
    // R
    public ArrayList<Dvd> getAllDvds();
    public Dvd getDvdById(int dvdId);
    // U
    public void updateDvd(Dvd dvd);
    // D
    public void removeDvd(int dvdId);
    
    public void load(String fileName);
    
    public void save();
    
    public List<Dvd> searchDvds(Map<SearchTerm, String> criteria);
    
}
