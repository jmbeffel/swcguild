/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.ops;

import com.sg.dvdlibrary.dto.Dvd;
import static com.sg.dvdlibrary.ops.DvdComparator.Order.Title;
import java.util.Comparator;

/**
 *
 * @author apprentice
 */
public class DvdComparator implements Comparator<Dvd> {

    public enum Order {
        Title, ReleaseDate
    }

    private Order sortBy = Title;

    @Override
    public int compare(Dvd o1, Dvd o2) {
        switch (sortBy) {
            case Title:
                return o1.getTitle().compareTo(o2.getTitle());
            case ReleaseDate:
                return o1.getReleaseDate().compareTo(o2.getReleaseDate());
        }
        throw new RuntimeException("Practically unreachable code, can't be thrown");
    }

    public void setSortBy(Order sortBy) {
        this.sortBy = sortBy;
    }

    public static Order getOrderFromString(String orderString) {
        switch (orderString.toLowerCase()) {
            case "title":
                return Order.Title;
            case "releasedate":
                return Order.ReleaseDate;
        }
        throw new RuntimeException("Practically unreachable code, can't be thrown");
    }
}
