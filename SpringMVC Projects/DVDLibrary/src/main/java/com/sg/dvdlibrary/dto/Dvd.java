/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.dto;

import java.util.ArrayList;
import java.util.Arrays;
import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Dvd{

    /**
     * 1. Design a DVD class to hold all the information about a DVD. It must
     * contain at least the following: a. Title b. Release date c. MPAA rating
     * d. Director's name e. Studio f. User rating or note (allows user to enter
     * additional information about the DVD, e.g. “Good family movie”)
     */
    private int dvdId;
    @NotEmpty(message = "Please enter a Title.")
    @Length(max = 50, message = "Title can not be longer than 50 characters")
    private String title;
    //@NotNull(message="Please enter a Release Date.")
    //@Min(value = 1900, message = "Only values greater than or equal to 1900 are accepted")
    //@Max(value = 2020, message = "Only values less than or equal to 2020 are accepted")
    // the "^$" means an empty string.
    // the "|" means or.
    // the "19\\d{2}" means a 4 digit number that starts with 19.
    // the "20\\d{2}" means a 4 digit number that starts with 20.
    @Pattern(regexp="19\\d{2}|200\\d|201\\d", message="Date must be a 4 digit year from 1900-2019")
    private String releaseDate;
    //@NotNull(message = "ENUM: Pick a valid MPAA Rating or leave blank")
    //private MpaaRating rating;
    @NotNull(message = "Pick a valid MPAA Rating or leave blank")
    private String mpaaRating;
    private String director;
    private String studio;
    private String note;
    //private static final ArrayList<String> MPAA_RATINGS = new ArrayList(Arrays.asList(new String[]{"","G", "PG", "PG-13", "R", "NC-17"}));

    public Dvd() {

    }
    
//    public void setRating(MpaaRating rating){
//        this.rating = rating;
//        //MpaaRating.fromString(rating.toUpperCase());
//    }
    
    /*
    public void setRating(String rating){
        this.rating = MpaaRating.fromString(rating);
        //MpaaRating.fromString(rating.toUpperCase());
    }
    
    public MpaaRating getRating(){
        return rating;
    }
    */

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the releaseDate
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * @param releaseDate the releaseDate to set
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * @return the MPAArating
     */
    public String getMpaaRating() {
        return mpaaRating;
    }

    /**
     * @param mpaaRating the MPAArating to set
     */
    public void setMpaaRating(String mpaaRating) {
        //if (!MPAA_RATINGS.contains(mpaaRating.toUpperCase())) {
//        if (!MpaaRating.contains(mpaaRating.toUpperCase())) {
//            this.mpaaRating = null;
//        } else {
//            this.mpaaRating = mpaaRating.toUpperCase();
//        }
        this.mpaaRating = MpaaRating.getLabelfromString(mpaaRating);
        if(this.mpaaRating.equals(MpaaRating.UNKNOWN.toString())){
            this.mpaaRating = null;
        }
    }

    /**
     * @return the director
     */
    public String getDirector() {
        return director;
    }

    /**
     * @param director the director to set
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     * @return the studio
     */
    public String getStudio() {
        return studio;
    }

    /**
     * @param studio the studio to set
     */
    public void setStudio(String studio) {
        this.studio = studio;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the dvdId
     */
    public int getDvdId() {
        return dvdId;
    }

    /**
     * @param dvdId the dvdId to set
     */
    public void setDvdId(int dvdId) {
        this.dvdId = dvdId;
    }

}
