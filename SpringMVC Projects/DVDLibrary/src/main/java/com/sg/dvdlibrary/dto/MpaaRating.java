/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public enum MpaaRating {
    UNKNOWN, NOT_RATED(""), G("G"), PG("PG"), PG13("PG-13"), R("R"), NC17("NC-17");
    private final String label;
    
    // <editor-fold defaultstate="collapsed" desc="Original set up of this Enum - written by me">
    
    //creating the hashmap to store all the key-value pairs
    private static final Map<String, MpaaRating> ratingsMap = new HashMap<>();
    //the static blick of code represents a class initializer.  without the static modifier, the code would be just an instance initializer
    //class initializers are executed when the class is loaded
    //instance inintializers are executed when the class is instantiated, immediately before the constructor code is executed
    //static initializers are like a method, but have no name, no arguments, no return type
    //since you never call it, it doesn't need a name and the only time it is called is when the class is loaded
    static {
        //add all the enums to the hashmap
        for(MpaaRating rating: values())
            ratingsMap.put(rating.label, rating);
    }
    //checking to see if the string value is paired to a valid enum
    public static boolean contains(String value){
        return ratingsMap.containsKey(value);
    }
    // </editor-fold>
    
    
    //These 2  folloring switch statements can take in a string and give back either the
    //enum that matches a given string, or the String label of the enum.
    public static String getLabelfromString(String value){
        switch(fromString(value)){
            case G:
                return G.label;
            case PG:
                return PG.label;
            case PG13:
                return PG13.label;
            case R:
                return R.label;
            case NC17:
                return NC17.label;
            case NOT_RATED:
                return NOT_RATED.label;
            default:
                return UNKNOWN.toString();
        }
    }
    
    public static MpaaRating fromString(String value){
        switch(value.toUpperCase()){
            case "G":
                return G;
            case "PG":
                return PG;
            case "PG-13":
            case "PG13":
            case "PG 13":
                return PG13;
            case "NC-17":
            case "NC17":
            case "NC 17":
                return NC17;
            case "":
                return NOT_RATED;
            default:
                return UNKNOWN;
        }
    }
    


    // <editor-fold defaultstate="collapsed" desc="Written By Jerry">
    
    
    public String getValue() {
        return toString();
    }
    
    public String getLabel() {
        return label;
    }
    
    // </editor-fold>
    
    //CONSTRUCTOR - input a string to set the stringValue
    private MpaaRating(String label){
        this.label = label;
    }
    private MpaaRating(){
        this.label = this.toString();
    }
}
