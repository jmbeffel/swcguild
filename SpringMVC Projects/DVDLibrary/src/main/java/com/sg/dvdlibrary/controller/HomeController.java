/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.controller;

import com.sg.dvdlibrary.dao.DvdDao;
import com.sg.dvdlibrary.dto.Dvd;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {
    
    private DvdDao dao;
    
    @Inject
    public HomeController(DvdDao dao){
        this.dao = dao;
    }
    
    // below are the list of controllers:
    //home, listCollection, search
    
    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String goHome() {
        return "home";
    }
    
    //get all dvds
    //request mapping - /dvds
    //reqponse body - list of dvds
    @RequestMapping(value = "/dvds", method = RequestMethod.GET)
    @ResponseBody public List<Dvd> getAllDvds(){
        //dao.load("dvd_library");
        return dao.getAllDvds();
    }
    
    //get a dvd (GET)
    //request mapping - /dvd/{id}
    //response body - dvd in JSON
    @RequestMapping(value = "/dvd/{id}", method = RequestMethod.GET)
    @ResponseBody public Dvd getDvd(@PathVariable("id") int id) {
        return dao.getDvdById(id);
    }
    
    //delete a Dvd (DELETE)
    //request mapping - /dvd/{id}
    @RequestMapping(value = "/dvd/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDvd(@PathVariable("id") int id) {
        dao.removeDvd(id);
    }
    
    //create a Dvd (POST)
    //request mapping - /dvd
    //request body - JSON object of the dvd
    //response body - JSON object of the dvd
    @RequestMapping(value = "/dvd", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Dvd createDvd(@Valid @RequestBody Dvd dvd){
        dao.addDvd(dvd);
        return dvd;
    }
    
    //update a Dvd (PUT)
    //request mapping - /dvd/{id}
    //request body - JSON object of the Dvd, with the changes to be made and the Dvd ID.
    @RequestMapping(value = "/dvd/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void editDvd(@PathVariable("id") int id, @Valid @RequestBody Dvd dvd){
        dvd.setDvdId(id);
        dao.updateDvd(dvd);
    }
}
