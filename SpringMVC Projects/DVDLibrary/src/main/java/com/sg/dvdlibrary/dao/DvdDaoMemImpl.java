/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.dao;

import com.sg.dvdlibrary.dto.SearchTerm;
import com.sg.dvdlibrary.dto.Dvd;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.core.io.ClassPathResource;

/**
 *
 * @author apprentice
 */
public class DvdDaoMemImpl implements DvdDao {

    private static final String DELIMITER = "::";
    private static final String DVD_LIBRARY = "dvdLibraries/testLibrary.txt";
    private HashMap<Integer, Dvd> dvdLibrary;
    private static int dvdId = 0;

    public DvdDaoMemImpl(){
        dvdLibrary = new HashMap<>();
        try{
            loadLibrary(DVD_LIBRARY);
        }catch(FileNotFoundException fnf){
            //file not found
        }catch(IOException ioe){
            //ioexception
        }
    }
    
    @Override
    public Dvd addDvd(Dvd dvd) {
        dvdId++;
        dvd.setDvdId(dvdId);
        dvdLibrary.put(dvdId, dvd);
        return dvd;
    }
    
    @Override
    public Dvd getDvdById(int dvdId) {
        return dvdLibrary.get(dvdId);
    }

    @Override
    public ArrayList<Dvd> getAllDvds() {
        return new ArrayList(dvdLibrary.values());
    }

    @Override
    public void updateDvd(Dvd dvd) {
        dvdLibrary.put(dvd.getDvdId(), dvd);
    }

    @Override
    public void removeDvd(int dvdId) {
        dvdLibrary.remove(dvdId);
    }

    @Override
    public void load(String fileName) {
        dvdLibrary.clear();
        StringBuilder fileLocation = new StringBuilder();
        fileLocation.append("dvdLibraries/");
        fileLocation.append(fileName);
        fileLocation.append(".txt");
        try{
            loadLibrary(fileLocation.toString());
        }catch(FileNotFoundException fnf){
            //file not found
        }catch(IOException ioe){
            //ioexception
        }
    }

    @Override
    public void save() {
        try{
            saveLibrary(DVD_LIBRARY);
        }catch(IOException ioe){
            
        }
    }
    
    @Override
    public List<Dvd> searchDvds(Map<SearchTerm, String> criteria){
        //first, the search function needs to get the string of the title being searched
        String searchCriteria = criteria.get(SearchTerm.TITLE);
        
        String[] searchWords = searchCriteria.split("\\s+");
        //predicate conditions are used by the filters
        Predicate<Dvd> titleMatches;
        Predicate<Dvd> releaseDateMatches;
        
        //default predicate - this is the general structure of assigning and filtering -
        //to be used when the search term is empty
        //Predicate<Object Type> predicate = (object) -> {do something with (object);};
        Predicate<Dvd> truePredicate = (dvd) -> {return true;};
        List<Dvd> dvds = new ArrayList<>(dvdLibrary.values());
        
        for(String word:searchWords){
            
        //assign the values to the predicates
        //searchMatches = boolean expression ? value when true : value when false;
        titleMatches = (word == null || word.isEmpty()) ? truePredicate : (dvd) -> dvd.getTitle().toLowerCase().contains(word.toLowerCase());
        releaseDateMatches = (word == null || word.isEmpty()) ? truePredicate : (dvd) -> dvd.getReleaseDate().contains(word);
        dvds = dvds.stream().filter(titleMatches.or(releaseDateMatches)).collect(Collectors.toList());
        }
        //make the list that contains everything containing the given search criteria
        return dvds;
    }

    private void loadLibrary(String filePath) throws FileNotFoundException, IOException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(new ClassPathResource(filePath).getFile())));
        String currentLine;
        String[] currentTokens;
        while (sc.hasNextLine()) {
            currentLine = sc.nextLine();
            currentTokens = currentLine.split(DELIMITER);
            Dvd currentDvd = new Dvd();
            // the file should store the following properties:
            // dvdId::title::releaseDate::MPAA::Director::studio::note
            //set the properties to the plpaceholder DVD
            currentDvd.setDvdId(Integer.parseInt(currentTokens[0]));
            currentDvd.setTitle(currentTokens[1]);
            currentDvd.setReleaseDate(currentTokens[2]);
            currentDvd.setMpaaRating(currentTokens[3]);
            currentDvd.setDirector(currentTokens[4]);
            currentDvd.setStudio(currentTokens[5]);
            currentDvd.setNote(currentTokens[6]);
            //add the current DVD to the hashmap
            dvdLibrary.put(currentDvd.getDvdId(), currentDvd);
        }
        sc.close();
        //get the max id number from the library, and set the next order number equal to that value
        dvdId = dvdLibrary.keySet().stream().reduce(0, Integer::max);
    }

    private void saveLibrary(String filePath) throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(new ClassPathResource(filePath).getFile()));
        dvdLibrary.values().stream().forEach((dvd) -> {
            //print out the properties to the file
            //make sure it is in the following format:
            // dvdId::title::releaseDate::MPAA::Director::studio::note
            out.println(dvd.getDvdId()
                    + DELIMITER + dvd.getTitle()
                    + DELIMITER + dvd.getReleaseDate()
                    + DELIMITER + dvd.getMpaaRating()
                    + DELIMITER + dvd.getDirector()
                    + DELIMITER + dvd.getStudio()
                    + DELIMITER + dvd.getNote());
        });
        //write to the file
        out.flush();        
        //close the writer
        out.close();
    }

}
