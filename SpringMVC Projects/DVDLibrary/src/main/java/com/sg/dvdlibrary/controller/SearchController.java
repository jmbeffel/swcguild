/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.controller;

import com.sg.dvdlibrary.dao.DvdDao;
import com.sg.dvdlibrary.dto.SearchTerm;
import com.sg.dvdlibrary.dto.Dvd;
import com.sg.dvdlibrary.ops.DvdComparator;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
public class SearchController{
    
    private DvdDao dao;
    private DvdComparator dvdCompare;
    
    @Inject
    public SearchController(DvdDao dao){
        this.dao = dao;
        dvdCompare = new DvdComparator();
    }
    
    //5. Allow the user to search for a DVD by title

    @RequestMapping(value = "/searchDvd", method = RequestMethod.GET)
    public String displaySearchPage(HttpServletRequest req, Model model) {
        List<Dvd> dvdLibrary = dao.getAllDvds();
        Collections.sort(dvdLibrary, dvdCompare);
        model.addAttribute("search", dvdLibrary);
        return "searchDvd";
    }
    
    @RequestMapping(value="/searchDvds", method= RequestMethod.POST)
    public String searchDvdsNoAjax(HttpServletRequest req, Model model){
        Map<SearchTerm, String> criteriaMap = new HashMap<>();
        
        String currentTerm = req.getParameter("title");
        List<Dvd> searchResults;
        if(!currentTerm.isEmpty()){
            criteriaMap.put(SearchTerm.TITLE, currentTerm);
            searchResults = dao.searchDvds(criteriaMap);
        } else{
            searchResults = dao.getAllDvds();
        }
        Collections.sort(searchResults, dvdCompare);
        model.addAttribute("searchTerm", currentTerm);
        model.addAttribute("search", searchResults);
        
        return "searchDvd";
    }
    
    //the method for searching with an ajax call
    @RequestMapping(value="search/dvds", method= RequestMethod.POST)
    @ResponseBody
    public List<Dvd> searchDvds(@RequestBody Map<String, String> searchMap){
        Map<SearchTerm, String> criteriaMap = new HashMap<>();
        
        String currentTerm = searchMap.get("title");
        if(!currentTerm.isEmpty()){
            criteriaMap.put(SearchTerm.TITLE, currentTerm);
            return dao.searchDvds(criteriaMap);
        }
        
//        currentTerm = searchMap.get("releaseDate");
//        if(!currentTerm.isEmpty()){
//            criteriaMap.put(SearchTerm.RELEASE_DATE, currentTerm);
//        }
//        
//        currentTerm = searchMap.get("mpaaRating");
//        if(!currentTerm.isEmpty()){
//            criteriaMap.put(SearchTerm.MPAA_RATING, currentTerm);
//        }
//        currentTerm = searchMap.get("director");
//        if(!currentTerm.isEmpty()){
//            criteriaMap.put(SearchTerm.DIRECTOR, currentTerm);
//        }
//        currentTerm = searchMap.get("studio");
//        if(!currentTerm.isEmpty()){
//            criteriaMap.put(SearchTerm.STUDIO, currentTerm);
//        }
//        currentTerm = searchMap.get("note");
//        if(!currentTerm.isEmpty()){
//            criteriaMap.put(SearchTerm.NOTE, currentTerm);
//        }
        return dao.getAllDvds();
    }

}
