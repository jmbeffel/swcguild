/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.dao;

import com.sg.dvdlibrary.dto.Dvd;
import com.sg.dvdlibrary.dto.SearchTerm;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class DvdDaoDbImpl implements DvdDao {

    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    
    // all SQL code is set into enums - IS THIS BEST PRACTICE?

    // if not, its still fun and really cool in java
    private enum SQL_DVD {
        
        INSERT("insert into Dvd (Title, ReleaseDate, MpaaRating, Director, Studio, Note) values (?, ?, ?, ?, ?, ?)"),
        DELETE("delete from Dvd where DvdId = ?"),
        UPDATE("update Dvd set Title = ?, ReleaseDate = ?, MpaaRating = ?, Director = ?, Studio = ?, Note = ? where DvdId = ?"),
        SELECT("select * from Dvd where DvdId = ?"),
        SELECT_ALL("select * from Dvd")
        ;
        
        private final String statement;
        
        private SQL_DVD(String command){
            this.statement = command;
        }
        
        //IS A GETTER NEEDED FOR A PRIVATE ENUM CLASS?
//        public String getCommand(){
//            return this.command;
//        }
        
    }
    
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Dvd addDvd(Dvd dvd) {
        //insert into the dvd table, and get the newly generated DvdId
        jdbcTemplate.update(SQL_DVD.INSERT.statement,
                dvd.getTitle(),
                dvd.getReleaseDate(),
                dvd.getMpaaRating(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getNote());
        dvd.setDvdId(jdbcTemplate.queryForObject("select last_insert_id()", Integer.class));
        return dvd;
    }

    @Override
    public ArrayList<Dvd> getAllDvds() {
        return new ArrayList<> (jdbcTemplate.query(SQL_DVD.SELECT_ALL.statement, new DvdMapper()));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Dvd getDvdById(int dvdId) {
        try{
            Dvd d = jdbcTemplate.queryForObject(SQL_DVD.SELECT.statement, new DvdMapper(), dvdId);
            return d;
        } catch (EmptyResultDataAccessException ex){
            return null;
        }
    }

    @Override
    public void updateDvd(Dvd dvd) {
        //update the dvd table
        jdbcTemplate.update(SQL_DVD.UPDATE.statement,
                dvd.getTitle(),
                dvd.getReleaseDate(),
                dvd.getMpaaRating(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getNote(),
                dvd.getDvdId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeDvd(int dvdId) {
        //delete dvd
        jdbcTemplate.update(SQL_DVD.DELETE.statement, dvdId);
    }

    @Override
    public void load(String fileName) {
        StringBuilder fileLocation = new StringBuilder();
        fileLocation.append("/dvdLibraries/");
        fileLocation.append(fileName);
        fileLocation.append(".sql");
        JdbcTestUtils.executeSqlScript(jdbcTemplate, new ClassPathResource(fileLocation.toString()), true);
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Dvd> searchDvds(Map<SearchTerm, String> criteria) {
        //first, the search function needs to get the string of the title being searched
        String searchCriteria = criteria.get(SearchTerm.TITLE);
        
        String[] searchWords = searchCriteria.split("\\s+");
        //predicate conditions are used by the filters
        Predicate<Dvd> titleMatches;
        Predicate<Dvd> releaseDateMatches;
        
        //default predicate - this is the general structure of assigning and filtering -
        //to be used when the search term is empty
        //Predicate<Object Type> predicate = (object) -> {do something with (object);};
        Predicate<Dvd> truePredicate = (dvd) -> {return true;};
        List<Dvd> dvds = new ArrayList<>(getAllDvds());
        
        for(String word:searchWords){
            
        //assign the values to the predicates
        //searchMatches = boolean expression ? value when true : value when false;
        titleMatches = (word == null || word.isEmpty()) ? truePredicate : (dvd) -> dvd.getTitle().toLowerCase().contains(word.toLowerCase());
        releaseDateMatches = (word == null || word.isEmpty()) ? truePredicate : (dvd) -> dvd.getReleaseDate().contains(word);
        dvds = dvds.stream().filter(titleMatches.or(releaseDateMatches)).collect(Collectors.toList());
        }
        //make the list that contains everything containing the given search criteria
        return dvds;
    }
    
    //Mapper
    private static final class DvdMapper implements RowMapper<Dvd> {

        @Override
        public Dvd mapRow(ResultSet rs, int rowNum) throws SQLException {
            Dvd dvd = new Dvd();
            dvd.setDvdId(rs.getInt("DvdId"));
            dvd.setTitle(rs.getString("Title"));
            dvd.setReleaseDate(rs.getString("ReleaseDate"));
            dvd.setMpaaRating(rs.getString("MpaaRating"));
            dvd.setDirector(rs.getString("Director"));
            dvd.setStudio(rs.getString("Studio"));
            dvd.setNote(rs.getString("Note"));
            return dvd;
        }
        
        
    }
    
    
}
